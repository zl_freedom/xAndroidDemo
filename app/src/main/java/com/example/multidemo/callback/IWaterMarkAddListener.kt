package com.example.multidemo.callback

import java.io.File

interface IWaterMarkAddListener {
    fun onSuccess(file: File)
}